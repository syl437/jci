// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngStorage','ngCordova','ionic.ion.imageCacheFactory','timer'])

.run(function($ionicPlatform,$rootScope,SendGetRequestServer,SendPostRequestServer) {
	
	//$rootScope.HostURL = 'http://ziv.bnyah.co.il/';
	$rootScope.HostURL = 'http://ziv.tapper.co.il/';
	//$rootScope.HostURL = 'http://localhost:8000/';
	//$rootScope.pushContent = '';
	$rootScope.CategoryArray = [];
	
  $ionicPlatform.ready(function() {
	  
	  
	  $rootScope.getCategories = function()
	  {
		    $rootScope.sendparams = 
			{
				"push_id" : $rootScope.pushId  // "91090585-595c-474a-96c6-babfb8b40763" // 
			};
			SendPostRequestServer.run($rootScope.sendparams,$rootScope.HostURL+'/GetCategoriesClient').then(function(data) {
				$rootScope.CategoryArray = data;
				//alert (JSON.stringify(data));
				console.log(data);
			});		
	  }
		  //if (!window.cordova)
			$rootScope.getCategories();
		
	  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	
	
  var notificationOpenedCallback = function(jsonData) {
	    
	  $rootScope.$broadcast('pushmessage',jsonData.message);	


		
	  //alert (jsonData.additionalData.type);
	  //$rootScope.pushContent = '';
	  $rootScope.pushContent = jsonData.message;
	  
	  if (!jsonData.isActive)
	  {
		  if (jsonData.additionalData.type == "pushmessage")
			 window.location ="#/app/pushmsg";
	  }


	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("11e0c946-2556-4be8-a113-105cafb905e7",
                                 {googleProjectNumber: "595323574268"},
                                 notificationOpenedCallback);

								 
			

  window.plugins.OneSignal.getIds(idsReceivedCallback);
  
	function idsReceivedCallback(ids) {
		
		$rootScope.pushId = ids.userId;
		//$rootScope.getCategories();

	}

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);


  
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.scrolling.jsScrolling(false);
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
	
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
	
    .state('app.phonebook', {
      url: '/phonebook',
      views: {
        'menuContent': {
          templateUrl: 'templates/phonebook.html',
          controller: 'PhonebookCtrl'
        }
      }
    })	

    .state('app.pushmsg', {
      url: '/pushmsg',
      views: {
        'menuContent': {
          templateUrl: 'templates/pushmsg.html',
          controller: 'PushmsgCtrl'
        }
      }
    })	
	
    .state('app.pushmessages', {
      url: '/pushmessages',
      views: {
        'menuContent': {
          templateUrl: 'templates/pushmessages.html',
          controller: 'PushMessagesCtrl'
        }
      }
    })		
	
  .state('app.details', {
    url: '/details/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/details.html',
        controller: 'DetailsCtrl'
      }
    }
  })
  
	
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
