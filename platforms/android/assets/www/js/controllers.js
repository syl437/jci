angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $localStorage,$ionicModal, $timeout,$rootScope,$ionicSideMenuDelegate,$ionicHistory,$ImageCacheFactory,$ionicLoading) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
	$scope.menuCategories = [];
  
	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.menuCategories = $rootScope.CategoryArray.categories;;
    });  
	
	$scope.navigateHome = function()
	{
		window.location ="#/app/main";
	}

	$scope.navigatePage = function(index)
	{
		
		/*
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
          noBackdrop : false,
          duration : 10000
        });
		

		$ImageCacheFactory.Cache([
			$rootScope.HostURL+$rootScope.CategoryArray[index].image
		]).then(function(){
			$ionicLoading.hide();
			window.location ="#/app/details/"+index;
		},function(failed){
			$ionicLoading.hide();
			window.location ="#/app/details/"+index;
		});	
	*/
				
		window.location ="#/app/details/"+index;
		
		
	}

	$scope.navigatePhone = function()
	{
		window.location ="#/app/phonebook";
	}

	
	$scope.logOut = function()
	{
		$localStorage.userid = '';
		window.location ="#/app/login";
	}
  	
	 $scope.toggleRightSideMenu = function() 
	 {
    	$ionicSideMenuDelegate.toggleRight();
  	 };
	 
	 $scope.GoBack = function() 
	 {
    	$ionicHistory.goBack();
    	//$rootScope.notifyIonicGoingBack();
  	 };
  
  

})


.controller('LoginCtrl', function($scope,$rootScope,$localStorage,SendGetRequestServer,SendPostRequestServer,$ionicPopup,$ionicSideMenuDelegate,$ionicHistory,$state) {

	$ionicSideMenuDelegate.canDragContent(false);
	
    if ($localStorage.userid)
    {
        $ionicHistory.nextViewOptions
		({
			disableAnimate: true,
			expire: 300,
			disableBack: true
        });
    
        $state.go('app.main');
    }

	

	$scope.loginfields = 
	{
		"mail" : "",
		"password" : ""
	}
	
	$scope.doLogin = function()
	{
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.loginfields.mail =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דוא"ל',
			 template: '',
			 buttons: [{
			 text: 'אישור',
			 type: 'button-positive',
			  }]			 
		   });				
		}
		else if (emailRegex.test($scope.loginfields.mail) == false)
		{

			$ionicPopup.alert({
			title: 'דוא"ל לא תקין יש לתקן',
			template: '',		
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.loginfields.mail =  '';
		}
			
		else if ($scope.loginfields.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: '',
			 buttons: [{
			 text: 'אישור',
			 type: 'button-positive',
			  }]			 
		   });				
		}
		else
		{
		    $scope.sendparams = 
			{
				"mail" : $scope.loginfields.mail,
				"password" : $scope.loginfields.password,
				"push_id" : $rootScope.pushId
			};
			SendPostRequestServer.run($scope.sendparams,$rootScope.HostURL+'/ValidateClientPassword').then(function(data) {
				
				
				
				if (data[0].status == 0)
				{
					$ionicPopup.alert({
					title: 'סיסמה שגויה יש לנסות שוב',
					template: '',		
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

				   $scope.loginfields.password =  '';					
				}
				else
				{
					
					$scope.loginfields.mail =  '';		
					$scope.loginfields.password =  '';	
					
					$localStorage.userid = data[0].userid;
					window.location ="#/app/main";
					
				}
			});	
		}
	}

})
.controller('MainCtrl', function($scope,$rootScope,$localStorage,SendGetRequestServer,SendPostRequestServer) {

	$scope.mainCategories = [];
	$scope.hostUrl = $rootScope.HostURL;
	
	$scope.date = new Date("05/23/2017 9:00:00"); 
	$scope.countdownseconds = $scope.date.getTime();


	

	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.mainCategories = $rootScope.CategoryArray.categories;
    });  

})


.controller('DetailsCtrl', function($scope,$stateParams,$rootScope,SendGetRequestServer,SendPostRequestServer,$ionicLoading) {

	$scope.hostUrl = $rootScope.HostURL;
	$scope.itemId = $stateParams.ItemId;
	$scope.InfoArray = $rootScope.CategoryArray.categories[$scope.itemId];
	//$scope.navTitle = $scope.InfoArray.title;
	//console.log("Details: " , $scope.InfoArray);

	$ionicLoading.show({
	  template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
	  noBackdrop : false,
	  duration : 10000
	});
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })	

})


.controller('PhonebookCtrl', function($scope,$stateParams,$rootScope,SendGetRequestServer,SendPostRequestServer,$ionicLoading,$ionicScrollDelegate) {

	$scope.hostUrl = $rootScope.HostURL;
	$scope.PhonebookArray = $rootScope.CategoryArray.phonebook;
	
	
	$scope.DialPhone = function(phone)
	{
		window.open('tel:' + phone, '_system');
	}
	
	$scope.BlurContacts = function()
	{
		$ionicScrollDelegate.scrollTop();
	}	
	
	
	$scope.CutNumber = function(number)
	{
		if (number)
		{
			$scope.SubPhone = number.substring(0, 6);
			if ($scope.SubPhone == "050843")
			{
				$scope.NewString = number.substring(6);
				return $scope.NewString;				
			}
			else
				return number;	
		}
	}
	
})

.controller('PushmsgCtrl', function($scope,$stateParams,$rootScope,SendGetRequestServer,SendPostRequestServer,$ionicLoading,$ionicScrollDelegate,$timeout) {

	$scope.hostUrl = $rootScope.HostURL;
	$scope.pushMessage = $rootScope.pushContent;

	$rootScope.$on('pushmessage', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			$scope.pushMessage = String(args);
		 }, 300);
	});

	
	
	$rootScope.$watch('pushContent', function(newValue, oldValue) {
		$scope.pushMessage = $rootScope.pushContent;
    });  

	
})



.controller('PushMessagesCtrl', function($scope,$stateParams,$rootScope,SendGetRequestServer,SendPostRequestServer,$ionicLoading,$ionicScrollDelegate,$timeout) {
	
	$scope.hostUrl = $rootScope.HostURL;
	$scope.mainMessages = [];

	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.mainMessages = $rootScope.CategoryArray.push_messages;
    }); 	
	
	
	
	$rootScope.$on('pushmessage', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			var date = new Date()
			var hours = date.getHours()
			var minutes = date.getMinutes()
		
			if (hours < 10)
			hours = " " + hours
			
			if (minutes < 10)
			minutes = "0" + minutes
		
			var time = hours+':'+minutes;

			$rootScope.CategoryArray.push_messages.unshift({
				"content": String(args),
				"date": "01/0/01 "+time
			});	 
			
			//$scope.mainMessages.reverse();
			
		 }, 300);	
	});


	
	$scope.GetTime = function(date)
	{
		$scope.splitDate = date.split(" ");
		$scope.SplitTime = $scope.splitDate[1].split(":");
		$scope.newTime = $scope.SplitTime[0]+':'+$scope.SplitTime[1];
		return $scope.newTime;
	}	

})

.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
});
